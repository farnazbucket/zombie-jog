//
//  GameScene.swift
//  zombie
//
//  Created by Mahammad on 2019-12-03.
//  Copyright © 2019 Mahammad. All rights reserved.
//


import UIKit
import CoreMotion
class ViewController: UIViewController {

    @IBOutlet weak var activityState: UILabel!
    @IBOutlet weak var steps: UILabel!
    
    var days:[String] = []
    var stepsTaken:[Int] = []
    
    @IBOutlet weak var stateImageView: UIImageView!
    let activityManager = CMMotionActivityManager()
    let pedoMeter = CMPedometer()
    
    override func viewDidLoad() {

        super.viewDidLoad()

        var cal = Calendar.current
        var comps = Calendar.dateComponents([.day, .month, .year, .weekday], from: Date())

        var comps = (Calendar.Unit.YearCalendarUnit | .MonthCalendarUnit | .DayCalendarUnit | .HourCalendarUnit | .MinuteCalendarUnit | .SecondCalendarUnit, fromDate: Date())
        comps.hour = 0
        comps.minute = 0
        comps.second = 0
        let timeZone = TimeZone.system
        cal.timeZone = timeZone
        
        let midnightOfToday = cal.dateFromComponents(comps)!
        

        if(CMMotionActivityManager.isActivityAvailable()){
            self.activityManager.startActivityUpdatesToQueue(OperationQueue.mainQueue, withHandler: { (data: CMMotionActivity!) -> Void in
               dispatch_async(dispatch_get_main_queue(), { () -> Void in
                if(data.stationary == true){
                    self.activityState.text = "Stationary"
                    self.stateImageView.image = UIImage(named: "Sitting")
                } else if (data.walking == true){
                    self.activityState.text = "Walking"
                    self.stateImageView.image = UIImage(named: "Walking")
                } else if (data.running == true){
                    self.activityState.text = "Running"
                    self.stateImageView.image = UIImage(named: "Running")
                } else if (data.automotive == true){
                    self.activityState.text = "Automotive"
                }
               })
                
           })
        }
        if(CMPedometer.isStepCountingAvailable()){
            let fromDate = NSDate(timeIntervalSinceNow: -86400 * 7)
            self.pedoMeter.queryPedometerDataFromDate(fromDate as Date, toDate: NSDate() as Date) { (data : CMPedometerData!, error) -> Void in
                println(data)
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    if(error == nil){
                        self.steps.text = "\(data.numberOfSteps)"
                    }
                })

            }
  
            self.pedoMeter.startPedometerUpdatesFromDate(midnightOfToday) { (data: CMPedometerData!, error) -> Void in
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    if(error == nil){
                        self.steps.text = "\(data.numberOfSteps)"
                    }
                })
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }


}
